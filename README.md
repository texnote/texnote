TeXNote
=======

_Like Death Note, but with TeX_

* [Socket.IO chat application](http://socket.io/get-started/chat/)
* [Real time collaboration with Web Sockets, NodeJS and Redis](http://www.laktek.com/2010/05/25/real-time-collaborative-editing-with-websockets-node-js-redis/)
* [Multi-instance NodeJS app in PaaS using Redis Pub/Sub](http://code.tutsplus.com/tutorials/multi-instance-nodejs-app-in-paas-using-redis-pubsub--cms-22239)
* [Dynamic MathJax from textbox](http://cdn.mathjax.org/mathjax/latest/test/sample-dynamic-2.html)
* [AngularJS directive for MathJax binding](http://stackoverflow.com/questions/16087146/getting-mathjax-to-update-after-changes-to-angularjs-model)
* [Computer Modern web fonts](http://checkmyworking.com/cm-web-fonts/)

Database: Redis
Server: NodeJS 
    API client: Express
    WebSockets: SocketIO
Client:
    JS Framework: AngularJS
    CSS Framework: Bootstrap

Landing page:
    Blurb
        really simple, no distraction notes
        limited support to mathjax rendering
        no images, no Tikz, nothing.
    Button to create new note
    (Later: username/some authentication?)

Note page:
    Controls fixed up top, opacity fade when typing
    White page, text black
    (Later: support keyboard shortcuts)
    keep input flow natural
        watch for any $$ or \[\] pairs for inline rendering
        click button to insert block level equation
    data is synchronized live
    store note as paragraph chunks
        lock paragraph while editing

Communication:
    Collaborators
        user id hash of collaborators
        mainly for reference
    Editing
        lock on paragraph - notify others
        small visual cue that a paragraph is being edited by whom
    Changes
        on every change (live editing) broadcast edits
        on paragraph 'exit' save to database
        only send differnce, or send paragraph ... saves on data transfer
    Save
        manual save button sends document to database for storage
        update last saved date-time
    Discussion
        Simple discussion interface between current collaborators
        first iteration: no saving to database
        later: can store in database with TTL
